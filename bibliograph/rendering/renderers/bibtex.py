# -*- coding: utf-8 -*-
###############################################################################
# $Copy$
###############################################################################
""" Bibtex render view

$Id: bibtex.py 121574 2010-07-22 11:33:38Z ajung $
"""
__docformat__ = 'reStructuredText'
__author__  = 'Tom Gross <itconsense@gmail.com>'

import os
import codecs
import tempfile
import logging

from zope.interface import implements

from bibliograph.core import utils
from bibliograph.core.utils import _normalize
from bibliograph.rendering.interfaces import IReferenceRenderer
from .base import BaseRenderer

###############################################################################

log = logging.getLogger('bibliograph.rendering')

###############################################################################

def _c(fmt, *args):

    try:
        return fmt % args
    except UnicodeDecodeError:
        args = tuple([repr(a) for a in args])
        return fmt % args


class BibtexRenderView(BaseRenderer):
    """A view rendering an IBibliographicReference to BibTeX.

    >>> from zope.interface.verify import verifyClass
    >>> verifyClass(IReferenceRenderer, BibtexRenderView)
    True

    """

    implements(IReferenceRenderer)

    file_extension = 'bib'

    def render(self, title_force_uppercase=False, omit_fields=[],
              msdos_eol_style=None, # not used
              resolve_unicode=None, # not used
              output_encoding=None, # not used
              ):
        """
        renders a BibliographyEntry object in BiBTex format
        """

        entry = self.context
        omit = [each.lower() for each in omit_fields]
        bib_key = utils._validKey(entry)
        f_temp = tempfile.mktemp()

        fp = codecs.open(f_temp, 'w', 'utf-8', 'ignore')
        print(_c("\n@%s{%s,",  entry.publication_type, bib_key), file=fp)

        if entry.editor_flag and self._isRenderableField('editor', omit):
            print(_c("  editor = {%s},", entry.authors), file=fp)
        elif not entry.editor_flag and self._isRenderableField('authors', omit):
            print(_c("  author = {%s},", entry.authors), file=fp)
        if self._isRenderableField('authorurls', omit):
            aURLs = utils.AuthorURLs(entry)
            if aURLs.find('http') > -1:
                print(_c("  authorURLs = {%s},", aURLs), file=fp)
        if self._isRenderableField('title', omit):
            if title_force_uppercase:
                print(_c("  title = {%s},", utils._braceUppercase(entry.title)), file=fp)
            else:
                print(_c("  title = {%s},", entry.title), file=fp)
        if self._isRenderableField('year', omit):
            print(_c("  year = {%s},", entry.publication_year), file=fp)
        if self._isRenderableField('month', omit):
            if entry.publication_month:
                print(_c("  month = {%s},", entry.publication_month), file=fp)
        if entry.url and self._isRenderableField('url', omit):
            print(_c("  URL = {%s},",  entry.url), file=fp)
        if entry.abstract and self._isRenderableField('abstract', omit):
            print(_c("  abstract = {%s},", entry.abstract), file=fp)

        for key, val in entry.source_fields:
            if self._isRenderableField(key, omit) and val:
                if not isinstance(val, str):
                    val = utils._decode(val)
                print(_c("  %s = {%s},",  key.lower(), val), file=fp)

        if self._isRenderableField('subject', omit):
            kws = ', '.join(entry.subject)
            if kws:
                if not isinstance(kws, str):
                    kws = utils._decode(kws)
                print(_c("  keywords = {%s},", kws), file=fp)
        if self._isRenderableField('note', omit):
            note = getattr(entry, 'note', None)
            if note:
                print(_c("  note = {%s},", note), file=fp)
        if self._isRenderableField('annote', omit):
            annote = getattr(entry, 'annote', None)
            if annote:
                print(_c("  annote = {%s},", annote), file=fp)
        if self._isRenderableField('additional', omit):
            try:
                additional = entry.context.getAdditional()
            except AttributeError:
                additional = []
            for mapping in additional:
                print(_c("  %s = {%s},", mapping['key'], mapping['value']), file=fp)

        keys = list(entry.identifiers.keys())
        keys.sort()
        source_fields_keys = [tp[0].lower() for tp in entry.source_fields]
        for identifier, value in list(entry.identifiers.items()):
            if value:
                print(_c("  %s = {%s},", identifier.lower(), value), file=fp)


        # remove trailing command
        fp.seek(-2, 2)
        print(file=fp)
        print("}", file=fp)
        print(file=fp)
        fp.close()
        bibtex = file(f_temp, 'r').read()
        os.unlink(f_temp)
        return _normalize(bibtex, resolve_unicode=True)

