.. contents::

.. bibliograph.rendering
   ---------------------

   Package contains renderers for different bibliographic formats. It uses an
   adapter to extract data from Zope/Plone-objects and provides utilities to call
   the renderers. Currently supported formats are: bibtex, endnote, pdf,
   xml (mods), and ris.

Homepage: http://pypi.python.org/pypi/bibliograph.rendering

Code repository: http://svn.plone.org/svn/collective/bibliograph.rendering/
